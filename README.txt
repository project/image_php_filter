Image PHP Filter
----------------

CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------

Standard PHP image filters as image styles effects.

Currently it implements:

- `IMG_FILTER_COLORIZE`
- `IMG_FILTER_CONTRAST`
- `IMG_FILTER_BRIGHTNESS`
- `IMG_FILTER_SMOOTH`
- `IMG_FILTER_GRAYSCALE`


REQUIREMENTS
------------

* Image module (core).


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Visit Configuration > Media > Image style
* Add or edit an image style to use these effects.


MAINTAINERS
-----------

Current maintainers:

* opi  - https://www.drupal.org/u/opi

This project has been sponsored by:

 * France-JustForYou
    Self-guided tours in France. Visit https://www.france-justforyou.com/ for
    more information.

Thanks to jayesh_makwana - https://www.drupal.org/user/3506773 for his help
